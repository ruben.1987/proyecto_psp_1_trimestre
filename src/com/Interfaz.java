package com;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Interfaz extends JFrame {
    public static int WIDTH = 500;
    public static int HEIGTH = WIDTH;
    private JPanel content_pane;
    private JButton btnStart;
    private JButton btnStop;
    private JButton btnResume;
    private JButton btnExt;
    private JButton btnInt;
    private JButton btnMiddle;
    private JButton btnExit;
    private JButton btnMoreSpeed;
    private JButton btnReduction;
    private JButton btnRed;
    private JButton btnBlue;
    private JButton btnGreen;
    private JButton btnOrange;
    private JButton btnViolet;
    private JButton button;
    private ArrayList<JButton> buttonList;
    ArrayList<Hilos> hiloLista;
    private JLabel lblFondo;
    private JComboBox cmbColores;
    private ArrayList<JButton> buttons;
    private ArrayList<Thread> hilos;
    private Hilos hilo1;
    private Hilos hilo2;
    private Hilos hilo3;
    private Hilos hilo4;
    private Hilos hilo5;

    /**
     * Constructor de la interfaz donde agregamos todos los componentes que vamos a utilizar.
     */
    public Interfaz() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGTH);
        content_pane = new JPanel();
        content_pane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(content_pane);
        content_pane.setLayout(null);
        lblFondo = new JLabel();
        lblFondo.setBounds(0, 0, WIDTH, HEIGTH);
        lblFondo.setIcon( new ImageIcon(new ImageIcon(Interfaz.class.getResource("/resources/glorieta.jpeg")).getImage()));
        btnStart = new JButton();
        btnStart.setBounds(5, 10, 110, 30);
        btnStart.setText("START");
        btnStop = new JButton();
        btnStop.setBounds(5, 40, 110, 30);
        btnStop.setText("STOP");
        btnResume = new JButton();
        btnResume.setBounds(5, 70, 110, 30);
        btnResume.setText("RESUME");
        btnExt = new JButton();
        btnExt.setBounds(55, 330, 110, 20);
        btnExt.setText("EXTERIOR");
        btnMiddle = new JButton();
        btnMiddle.setBounds(55, 350, 110, 20);
        btnMiddle.setText("MEDIO");
        btnInt = new JButton();
        btnInt.setBounds(55, 370, 110, 20);
        btnInt.setText("INTERIOR");
        btnExit = new JButton();
        btnExit.setText("EXIT");
        btnExit.setBounds(55, 390, 110, 20);
        btnMoreSpeed = new JButton();
        btnMoreSpeed.setText("RAPIDO");
        btnMoreSpeed.setBounds(55, 410, 110, 20);
        btnReduction = new JButton();
        btnReduction.setText("LENTO");
        btnReduction.setBounds(55, 430, 110, 20);
        btnRed = new JButton();
        btnRed.setBackground(Color.red);
        btnRed.setSize(12, 12);
        btnBlue = new JButton();
        btnBlue.setBackground(Color.blue);
        btnBlue.setSize(12, 12);
        btnGreen = new JButton();
        btnGreen.setSize(12, 12);
        btnGreen.setBackground(Color.green);
        btnOrange = new JButton();
        btnOrange.setSize(12, 12);
        btnOrange.setBackground(Color.orange);
        btnViolet = new JButton();
        btnViolet.setSize(12, 12);
        btnViolet.setBackground(Color.magenta);
        buttonList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            button = new JButton();
            button.setBounds( (i * 10), 240, 10, 10);
            button.setBackground(Color.black);
            buttonList.add(button);
            content_pane.add(button);
        }
        cmbColores = new JComboBox();
        cmbColores.setBounds(5, 330, 50, 20);
        cmbColores.addItem("All");
        cmbColores.addItem("Red");
        cmbColores.addItem("Blue");
        cmbColores.addItem("Green");
        cmbColores.addItem("Orange");
        cmbColores.addItem("Violet");
        btnStop.setEnabled(false);
        btnResume.setEnabled(false);
        btnInt.setEnabled(false);
        btnMiddle.setEnabled(false);
        btnExt.setEnabled(false);
        btnMoreSpeed.setEnabled(false);
        btnReduction.setEnabled(false);
        btnExit.setEnabled(false);
        content_pane.add(btnRed);
        content_pane.add(btnBlue);
        content_pane.add(btnGreen);
        content_pane.add(btnOrange);
        content_pane.add(btnViolet);
        content_pane.add(btnStart);
        content_pane.add(btnStop);
        content_pane.add(btnResume);
        content_pane.add(btnExt);
        content_pane.add(btnInt);
        content_pane.add(btnMiddle);
        content_pane.add(btnExit);
        content_pane.add(btnMoreSpeed);
        content_pane.add(btnReduction);
        content_pane.add(cmbColores);
        content_pane.add(lblFondo);
        rotate();

        /**
         * Evento que pone en marcha todos los Thread pasandole un JButton diferente a cada uno.
         */
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnExit.setEnabled(true);
                btnStop.setEnabled(true);
                btnMoreSpeed.setEnabled(true);
                btnReduction.setEnabled(true);
                btnInt.setEnabled(true);
                btnMiddle.setEnabled(true);
                btnExt.setEnabled(true);
                hilo1 = new Hilos(btnRed);
                hilo2 = new Hilos(btnBlue);
                hilo3 = new Hilos(btnGreen);
                hilo4 = new Hilos(btnOrange);
                hilo5 = new Hilos(btnViolet);
                hilo1.start();
                hilo2.start();
                hilo3.start();
                hilo4.start();
                hilo5.start();
                for (Hilos p : hiloLista) {
                    p.start();
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                btnStart.setEnabled(false);
            }
        });


        /**
         * Evento que pausa la ejecución de los Thread, usando monitores.
         */
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnResume.setEnabled(true);
                btnStart.setEnabled(false);
                btnMoreSpeed.setEnabled(false);
                btnReduction.setEnabled(false);
                btnInt.setEnabled(false);
                btnMiddle.setEnabled(false);
                btnExt.setEnabled(false);
                hilo1.stopHilo();
                hilo2.stopHilo();
                hilo3.stopHilo();
                hilo4.stopHilo();
                hilo5.stopHilo();
            }
        });

        /**
         * Evento que reanuda la ejecución de los Threads.
         */
        btnResume.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnMoreSpeed.setEnabled(true);
                btnReduction.setEnabled(true);
                btnInt.setEnabled(true);
                btnMiddle.setEnabled(true);
                btnExt.setEnabled(true);
                hilo1.resumeHilo();
                hilo2.resumeHilo();
                hilo3.resumeHilo();
                hilo4.resumeHilo();
                hilo5.resumeHilo();
                btnResume.setEnabled(false);
            }
        });

        /**
         * Evento que comunica al Thread seleccionado (a través del JButton que ejecuta) el cambio de carril exterior.
         */
        btnExt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.exterior();
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.exterior();
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.exterior();
                    }else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.exterior();
                    }else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.exterior();
                    }
                }else if(cmbColores.getSelectedItem().equals("All")){
                    hilo1.exterior();
                    hilo2.exterior();
                    hilo3.exterior();
                    hilo4.exterior();
                    hilo5.exterior();
                }
            }
        });

        btnMiddle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.middle();
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.middle();
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.middle();
                    }else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.middle();
                    }else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.middle();
                    }
                }else if(cmbColores.getSelectedItem().equals("All")){
                    hilo1.middle();
                    hilo2.middle();
                    hilo3.middle();
                    hilo4.middle();
                    hilo5.middle();
                }
            }
        });


        btnMoreSpeed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.moreSpeed();
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.moreSpeed();
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.moreSpeed();
                    }else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.moreSpeed();
                    }else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.moreSpeed();
                    }
                }else if(cmbColores.getSelectedItem().equals("All")){
                    hilo1.moreSpeed();
                    hilo2.moreSpeed();
                    hilo3.moreSpeed();
                    hilo4.moreSpeed();
                    hilo5.moreSpeed();
                }
            }
        });

        btnReduction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.lessSpeed();
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.lessSpeed();
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.lessSpeed();
                    }else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.lessSpeed();
                    }else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.lessSpeed();
                    }
                }else if(cmbColores.getSelectedItem().equals("All")){
                    hilo1.lessSpeed();
                    hilo2.lessSpeed();
                    hilo3.lessSpeed();
                    hilo4.lessSpeed();
                    hilo5.lessSpeed();
                }
            }
        });


        /**
         * Evento que comunica al Thread seleccionado (a través del JButton que ejecuta) el cambio de carril interior.
         */
        btnInt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.interior();
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.interior();
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.interior();
                    } else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.interior();
                    } else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.interior();
                    }
                }else if(cmbColores.getSelectedItem().equals("All")){
                    hilo1.interior();
                    hilo2.interior();
                    hilo3.interior();
                    hilo4.interior();
                    hilo5.interior();
                }
            }
        });

        /**
         * Evento que comunica la la salida por el carril mas cercano.
         */
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!cmbColores.getSelectedItem().equals("All")) {
                    if (cmbColores.getSelectedItem().equals("Red")) {
                        hilo1.stopHilo();
                        hilo1.resumeHilo();
                        hilo1.exitButtonToColor();
                        cmbColores.removeItem("Red");
                        cmbColores.setSelectedIndex(0);
                    } else if (cmbColores.getSelectedItem().equals("Blue")) {
                        hilo2.stopHilo();
                        hilo2.resumeHilo();
                        hilo2.exitButtonToColor();
                        cmbColores.removeItem("Blue");
                        cmbColores.setSelectedIndex(0);
                    } else if (cmbColores.getSelectedItem().equals("Green")) {
                        hilo3.stopHilo();
                        hilo3.resumeHilo();
                        hilo3.exitButtonToColor();
                        cmbColores.removeItem("Green");
                        cmbColores.setSelectedIndex(0);
                    }else if (cmbColores.getSelectedItem().equals("Orange")) {
                        hilo4.stopHilo();
                        hilo4.resumeHilo();
                        hilo4.exitButtonToColor();
                        cmbColores.removeItem("Orange");
                        cmbColores.setSelectedIndex(0);
                    }else if (cmbColores.getSelectedItem().equals("Violet")) {
                        hilo5.stopHilo();
                        hilo5.resumeHilo();
                        hilo5.exitButtonToColor();
                        cmbColores.removeItem("Violet");
                        cmbColores.setSelectedIndex(0);
                    }
                }
            }
        });
    }

    /**
     * Método que asigna posiciones a cada uno de los JButton.
     */
    public void rotate() {
        double x = 230, y = 400;
        Point p = new Point();
        p.setLocation(x, y);
        btnRed.setLocation(p);
        y = 425;
        p.setLocation(x, y);
        btnBlue.setLocation(p);
        y = 450;
        p.setLocation(x, y);
        btnGreen.setLocation(p);
        y = 475;
        p.setLocation(x, y);
        btnOrange.setLocation(p);
        y = 500;
        p.setLocation(x, y);
        btnViolet.setLocation(p);
        hiloLista = new ArrayList<>();
        Hilos hilo;
        for (int i = 0; i < buttonList.size(); i++) {
            hilo = new Hilos(buttonList.get(i));
            hiloLista.add(hilo);
        }
    }
}
