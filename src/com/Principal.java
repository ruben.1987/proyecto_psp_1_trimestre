package com;
import javax.swing.*;
import java.awt.*;

public class Principal {
    public static void main(String[] args) {
        /**
         * Evento que lanza la interfaz gráfica con todos los componentes añadidos.
         */
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Interfaz interfaz = new Interfaz();
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    interfaz.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
