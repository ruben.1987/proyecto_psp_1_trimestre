package com;

import javax.swing.*;
import java.awt.*;

public class Hilos extends Thread {
    public static final double MOST_INCREMENT = 0.15;
    public static final double INCREMENT_EXT=0.30;
    public static final double SPEED_EXIT = 0.000005;
    public static final double RADIO_INT = 80;
    public static final double RADIO_MEDIO = 100;
    public static final double RADIO_EXT = 120;
    public static final double RADIO_EXT_WHITE_BUTTONS = 135;
    private JButton btnRotate;
    private boolean stopHilos;
    private boolean exit;
    private boolean runInt;
    private boolean runMiddle;
    private boolean runExt;
    private boolean execute;
    private double speed=MOST_INCREMENT;
    private Point p = new Point();
    private double thetaInt = Math.toRadians(270);
    private double thetaExt = Math.toRadians(0);

    public Hilos(JButton btnRotate) {
        this.btnRotate = btnRotate;
    }

    /**
     * Método que pone en ejecucion cada uno de los hilos que manejaran nuestros JButton.
     */
    @Override
    public synchronized void run() {
        stopHilos = true;
        exit = false;
        runInt=true;
        runMiddle=false;
        runExt = false;
        execute = true;
        double x = btnRotate.getX(), y = btnRotate.getY();
        if (btnRotate.getX() < 50) {
            while (btnRotate.getX() <= 120) {
                x += 1;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                try {
                    sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            x = 240;
            y = 240;
            while (execute) {
                try {
                    rotateButtonsExt(x, y);
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            while (btnRotate.getY() >= 330) {
                y -= 1;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                try {
                    sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            x = 240;
            y = 240;
            while (execute) {
                try {
                    while (!runExt && !runMiddle && runInt && execute) {
                        rotateInt(x, y);
                        sleep(50);
                        if (!stopHilos) wait();
                    }
                    while (!runExt && runMiddle && !runInt && execute) {
                        rotateMedio(x, y);
                        sleep(50);
                        if (!stopHilos) wait();
                    }
                    while (runExt && !runMiddle && !runInt && execute) {
                        rotateExt(x, y);
                        sleep(50);
                        if (!stopHilos) wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que hará girar en un circulo sobre un punto dado por el carril interior.
     *
     * @param x posición x del JButton.
     * @param y posición y del JButton.
     */
    public void rotateInt(double x, double y) {
        double mX = (int) ((Math.cos(thetaInt)) * RADIO_INT);
        double mY = (int) ((Math.sin(thetaInt)) * RADIO_INT);

        x -= mX;
        y -= mY;

        thetaInt-=speed;
        p.setLocation(x, y);
        btnRotate.setLocation(p);

        if (exit) exitButton(x, y, mX, mY);
    }

    public void rotateMedio(double x, double y) {
        double mX = (int) ((Math.cos(thetaInt)) * RADIO_MEDIO);
        double mY = (int) ((Math.sin(thetaInt)) * RADIO_MEDIO);

        x -= mX;
        y -= mY;

        thetaInt-=speed;
        p.setLocation(x, y);
        btnRotate.setLocation(p);

        if (exit) exitButton(x, y, mX, mY);
    }

    /**
     * Método que hará girar en un circulo sobre un punto dado por el carril exterior.
     *
     * @param x posición x del JButton.
     * @param y posición y del JButton.
     */
    public void rotateExt(double x, double y) {
        double mX = (int) ((Math.cos(thetaInt)) * RADIO_EXT);
        double mY = (int) ((Math.sin(thetaInt)) * RADIO_EXT);

        x -= mX;
        y -= mY;

        thetaInt-=speed;
        p.setLocation(x, y);
        btnRotate.setLocation(p);

        if (exit) exitButton(x, y, mX, mY);
    }

    /**
     * Método que hará girar en un circulo sobre un punto dado por el carril exteriora los JButton no manejables.
     *
     * @param x posición x del JButton.
     * @param y posición y del JButton.
     */
    public void rotateButtonsExt(double x, double y) {
        double mX = (int) ((Math.cos(thetaExt)) * RADIO_EXT_WHITE_BUTTONS);
        double mY = (int) ((Math.sin(thetaExt)) * RADIO_EXT_WHITE_BUTTONS);
        x -= mX;
        y -= mY;
        thetaExt += INCREMENT_EXT;
        p.setLocation(x, y);
        btnRotate.setLocation(p);
        if (exit) exitButton(x, y, mX, mY);
    }

    /**
     * Método que saca a los JButton por la salida más cercana.
     *
     * @param x  posición x del JButton.
     * @param y  posición y del JButton.
     * @param mX cambio de posición medido en radianes del eje x.
     * @param mY cambio de posición medido en radianes del eje y.
     */
    public void exitButton(double x, double y, double mX, double mY) {
        if (Math.toRadians(mX) < 0 && Math.toRadians(mY) > 1) {
            while (execute) {
                y -= SPEED_EXIT;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                if (y < -30) end();
            }
        } else if (Math.toRadians(mX) < -1 && Math.toRadians(mY) > 0) {
            while (execute) {
                x += SPEED_EXIT;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                if (x > 600) end();
            }
        } else if (Math.toRadians(mX) > 1 && Math.toRadians(mY) < 0) {
            while (execute) {
                x -= SPEED_EXIT;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                if (x < -30) end();
            }
        } else if (Math.toRadians(mX) > -1 && Math.toRadians(mY) < -1) {
            while (execute) {
                y += SPEED_EXIT;
                p.setLocation(x, y);
                btnRotate.setLocation(p);
                if (y > 600) end();
            }
        }
    }

    /**
     * Método que cambia de valor el boolean que controla la parada de los hilos en ejecución.
     */
    public void stopHilo() {
        this.stopHilos = false;
    }

    /**
     * Método que cambia de valor el boolean que controla la parada de los hilos en ejecución, notificando su reanudación.
     */
    public synchronized void resumeHilo() {
        this.stopHilos = true;
        notify();
    }


    /**
     * Método que comunica al hilo seleccionado que debe coger por la salida más cerceana.
     */
    public void exitButtonToColor() {
        exit = true;
    }

    /**
     * Método que comunica al hilo que ha salido del Frame que debe dejar de ejecutarse.
     */
    public void end() {
        execute = false;
    }

    /**
     * Método qque cambia alJButton seleccionado el cambio de carril al exterior y el aumento de velocidad.
     */
    public void exterior() {
        if (runMiddle) {
            runMiddle = false;
        }
        if(runInt){
            runInt=false;
        }
        this.runExt = true;
    }


    public void middle() {
        if (runExt) {
            runExt = false;
        }
        if(runInt){
            runInt=false;
        }
        runMiddle = true;
    }

    public void interior() {
        if (runExt) {
            runExt = false;
        }
        if (runMiddle) {
            runMiddle = false;
        }
        runInt=true;
    }

    public void moreSpeed(){
        if(speed<=MOST_INCREMENT*4){
            speed+=MOST_INCREMENT;
        }
    }

    public void lessSpeed(){
        if(speed>MOST_INCREMENT){
            speed-=MOST_INCREMENT;
        }
    }
}

